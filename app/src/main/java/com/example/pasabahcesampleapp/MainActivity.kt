package com.example.pasabahcesampleapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.poilabs.navigation.model.PoiNavigation
import com.poilabs.navigation.view.fragments.MapFragment
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    var poiNavigation = PoiNavigation()
    var localeLanguage = Locale.forLanguageTag(Locale.getDefault().language).toString()
    var storeIdList = arrayListOf("4")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askLocalPermission()
        } else {
            startScan()
        }

        btn_get_store.setOnClickListener {
            poiNavigation.updateStoreList(storeIdList)
        }
    }

    private fun askLocalPermission() {
        val hasLocalPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
        if (hasLocalPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_COARSE_LOCATION2)
        } else {
            startScan()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_COARSE_LOCATION2 -> if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                // Permission Granted
                startScan()
            } else {
                // Permission Denied
                finish()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun startScan() {

        poiNavigation.init(this, "bdeac7eb-f2af-4d4c-be03-4f71cca357d1", "f1411dac-f7d7-459e-a078-769eca4cb631", "Pasabahce",localeLanguage)
        // poiNavigation.init(this, "bdeac7eb-f2af-4d4c-be03-4f71cca357d1", "f1411dac-f7d7-459e-a078-769eca4cb631", "Pasabahce",localeLanguage, storeIdList ) // init with store id
        poiNavigation.bind(object : PoiNavigation.OnNavigationReady {
            override fun onReady(mapFragment: MapFragment?) {
                mapFragment ?: return
                val bundle=Bundle()
                mapFragment.arguments = bundle
                supportFragmentManager.beginTransaction().replace(R.id.map,mapFragment)
                    .commitAllowingStateLoss()
            }
        })
    }




}

const val PERMISSION_REQUEST_COARSE_LOCATION2 = 88